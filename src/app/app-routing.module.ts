import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { DetailsComponent } from './details/details.component';
import { PostsComponent } from './posts/posts.component';
import { EditSpecsComponent } from './edit-specs/edit-specs.component';
import { SurveyFormsComponent } from './survey-forms/survey-forms.component';
import { ReportsComponent } from './reports/reports.component';
import { DashboardsComponent } from './dashboards/dashboards.component';
import { LoginsComponent } from './logins/logins.component';

const routes: Routes = [
  {
    path: '',
    component: LoginsComponent
  },
  {
    path: 'details/:id',
    component: DetailsComponent
  },
  {
    path: 'posts',
    component: PostsComponent
  },
  {
    path: 'survey-forms',
    component: SurveyFormsComponent
  },
  {
    path: 'edit-specs',
    component: EditSpecsComponent
  },
  {
    path: 'reports',
    component: ReportsComponent
  },
  {
    path: 'dashboards',
    component: DashboardsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
